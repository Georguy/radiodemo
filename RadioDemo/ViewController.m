//
//  ViewController.m
//  RadioDemo
//
//  Created by Georgij on 20.10.16.
//  Copyright © 2016 Georgii Emeljanow. All rights reserved.
//

#import "ViewController.h"
#import "RadioKit.h"
#import "BufferView.h"
#import <MediaPlayer/MediaPlayer.h>

#define CONNECT_RADIO_STRING @"Playing"
#define STOP_RADIO_STRING @"Press play button"
#define PAUSE_RADIO_STRING @"Pause. Press play button"
#define STREAM_URL @"http://ice2.gocaster.net/sw"

@interface ViewController () <StormysRadioKitDelegate>

@property (strong, nonatomic) RadioKit* radioKit;
@property (strong, nonatomic) NSTimer* bufferViewTimer;
@property (strong, nonatomic) NSTimer* rewOrFFTimer;

@property (weak, nonatomic) IBOutlet UILabel* netStatusLbl;
@property (weak, nonatomic) IBOutlet BufferView* bufferView;

@property (weak, nonatomic) IBOutlet UIButton* playBtn;
@property (weak, nonatomic) IBOutlet UIButton* rewBtn;
@property (weak, nonatomic) IBOutlet UIButton* ffBtn;

@property (weak, nonatomic) IBOutlet UILabel* bufferLbl;

@end

@implementation ViewController {
    time_t _prevNoNetworkWarning;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _radioKit = [[RadioKit alloc] init];
    [_radioKit authenticateLibraryWithKey1:0x1234 andKey2:0x1234];
    
    _radioKit.delegate = self;
    
    [_radioKit setBufferWaitTime:15];
    [_radioKit setDataTimeout:10];
    [_radioKit setStreamUrl:STREAM_URL isFile:NO];
    [self startBufferViewThread];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self updateAudioButtons];
    [self updateStatusString];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Audio Control Buttons -
- (void) updateAudioButtons {
    if ([_radioKit getStreamStatus] != SRK_STATUS_STOPPED && [_radioKit getStreamStatus] != SRK_STATUS_PAUSED){
        
        [self setButtonToStopImage];
        self.rewBtn.enabled = YES;
        
        if ([_radioKit isFastForwardAllowed:10]){
            self.ffBtn.enabled = YES;
        }else{
            self.ffBtn.enabled = NO;
        }
    }else{
        [self setButtonToPlayImage];
        self.rewBtn.enabled = NO;
        self.ffBtn.enabled = NO;
    }
}

- (void) setButtonToPlayImage {
    [self.playBtn setImage:[UIImage imageNamed:@"play"]  forState:UIControlStateNormal];
}


- (void) setButtonToStopImage {
    [self.playBtn setImage:[UIImage imageNamed:@"pause"]  forState:UIControlStateNormal];
}


#pragma mark - Status -
- (void) updateStatusString {
    switch([_radioKit getStreamStatus]){
            
        case SRK_STATUS_STOPPED:
            [self.netStatusLbl performSelectorOnMainThread:@selector(setText:) withObject:STOP_RADIO_STRING waitUntilDone:NO];
            break;
        case SRK_STATUS_CONNECTING:
            [self.netStatusLbl performSelectorOnMainThread:@selector(setText:) withObject:@"Connecting..." waitUntilDone:NO];
            break;
        case SRK_STATUS_BUFFERING:
            [self.netStatusLbl performSelectorOnMainThread:@selector(setText:) withObject:@"Buffering..." waitUntilDone:NO];
            break;
        case SRK_STATUS_PLAYING:
            if (_radioKit.currTitle != nil){
                [self.netStatusLbl performSelectorOnMainThread:@selector(setText:) withObject:_radioKit.currTitle waitUntilDone:NO];
            }else{
                [self.netStatusLbl performSelectorOnMainThread:@selector(setText:) withObject:CONNECT_RADIO_STRING waitUntilDone:NO];
            }
            break;
        case SRK_STATUS_PAUSED:
            break;
    }				
}

#pragma mark - Buffer Visualizer -
- (void)startBufferViewThread {
    _bufferViewTimer = [NSTimer scheduledTimerWithTimeInterval:0.2f target:self selector:@selector(bufferVisualThread) userInfo:nil repeats:YES];
}

- (void) bufferVisualThread {
    _bufferView.bufferSizeSRK = [_radioKit maxBufferSize];
    _bufferView.bufferCountSRK = [_radioKit currBufferUsage];
    _bufferView.currBuffPtr = [_radioKit currBufferPlaying];
    _bufferView.bufferByteOffset = [_radioKit bufferByteOffset];
    
    [_bufferView performSelectorOnMainThread:@selector(setNeedsDisplay) withObject:nil waitUntilDone:YES];
    
    if ([_radioKit getStreamStatus] == SRK_STATUS_BUFFERING){
        NSInteger buffTimeBeforeStart = [_radioKit bufferWaitTime] - [_radioKit currBufferUsageInSeconds];
        if (buffTimeBeforeStart > 0){
            [self.bufferLbl performSelectorOnMainThread:@selector(setText:)
                                          withObject:[NSString stringWithFormat:@"Buffering: %ld", (long)buffTimeBeforeStart]
                                       waitUntilDone:YES];
        }else{
            [self.bufferLbl performSelectorOnMainThread:@selector(setText:)
                                          withObject:@""
                                       waitUntilDone:YES];
        }
    }else if (![self.bufferLbl.text isEqualToString:@""]){
        [self.bufferLbl performSelectorOnMainThread:@selector(setText:)
                                      withObject:@"" 
                                   waitUntilDone:YES];
    }		
    
}


#pragma mark - StormysRadioKitDelegate -
- (void)SRKConnecting {
    [self performSelectorOnMainThread:@selector(updateAudioButtons) withObject:nil waitUntilDone:NO];
    [self.netStatusLbl performSelectorOnMainThread:@selector(setText:) withObject:@"Connecting..." waitUntilDone:NO];
}

- (void)SRKIsBuffering {
    [self.netStatusLbl performSelectorOnMainThread:@selector(setText:) withObject:@"Buffering..." waitUntilDone:NO];
}

- (void)SRKPlayStarted {
    [_radioKit enableLevelMetering];
    
    if (_radioKit.currTitle != nil){
        [self.netStatusLbl performSelectorOnMainThread:@selector(setText:) withObject:_radioKit.currTitle waitUntilDone:NO];
    }else{
        [self.netStatusLbl performSelectorOnMainThread:@selector(setText:) withObject:CONNECT_RADIO_STRING waitUntilDone:NO];
    }
    
    [self performSelectorOnMainThread:@selector(updateAudioButtons) withObject:nil waitUntilDone:NO];
}

- (void)SRKPlayStopped {
    [self performSelectorOnMainThread:@selector(updateAudioButtons) withObject:nil waitUntilDone:NO];
    [self.netStatusLbl performSelectorOnMainThread:@selector(setText:) withObject:STOP_RADIO_STRING waitUntilDone:NO];
}

- (void)SRKPlayPaused {
    [self performSelectorOnMainThread:@selector(updateAudioButtons) withObject:nil waitUntilDone:NO];
    [self.netStatusLbl performSelectorOnMainThread:@selector(setText:) withObject:PAUSE_RADIO_STRING waitUntilDone:NO];
}


- (void)SRKNoNetworkFound {
    if (_prevNoNetworkWarning == 0){
        _prevNoNetworkWarning = time(NULL);
        return;
    }
    if (time(NULL) - _prevNoNetworkWarning < 2.0f || [_radioKit isAudioPlaying]){
        return;
    }
    
    [_radioKit stopStream];
    [self performSelectorOnMainThread : @selector(showNoNetworkAlert) withObject:nil waitUntilDone:YES];
}

- (void) showNoNetworkAlert {
    UIAlertView *baseAlert = [[UIAlertView alloc]
                              initWithTitle:@"No Network" message:@"A network connection is required.  Please verify your network settings and try again."
                              delegate:nil cancelButtonTitle:nil
                              otherButtonTitles:@"OK", nil];
    [self.playBtn setImage:[UIImage imageNamed:@"play"]  forState:UIControlStateNormal];
    [baseAlert show];
}

#pragma mark - IBActions -
- (IBAction) playOrStop: (id) sender {
    int currStatus = [_radioKit getStreamStatus];
    
    if (currStatus == SRK_STATUS_STOPPED || currStatus == SRK_STATUS_PAUSED){
        [_radioKit startStream];
    }else if (currStatus == SRK_STATUS_PLAYING){
        [_radioKit pauseStream];
    }else{
        [_radioKit stopStream];
    }
    [self updateAudioButtons];
}

- (IBAction)rewindDown:(id)sender {
    [self rewind];
    _rewOrFFTimer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(rewind) userInfo:nil repeats:YES];
}

- (IBAction)rewindUp:(id)sender {
    if (_rewOrFFTimer != nil) {
        [_rewOrFFTimer invalidate];
    }
    _rewOrFFTimer = nil;
}

- (void) rewind {
    [_radioKit rewind: 10];
    [self updateAudioButtons];
}

- (IBAction)fastForwardDown:(id)sender {
    [self fastForward];
    _rewOrFFTimer = [NSTimer scheduledTimerWithTimeInterval:0.3 target:self selector:@selector(fastForward) userInfo:nil repeats:YES];
}

- (IBAction)fastForwardUp:(id)sender {
    if (_rewOrFFTimer != nil) {
        [_rewOrFFTimer invalidate];
    }
    _rewOrFFTimer = nil;
}

- (void) fastForward {
    [_radioKit fastForward: 10];
    [self updateAudioButtons];
}


@end
